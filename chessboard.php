<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>square</title>
    <style>
        .odd_div,.even_div
        {
            width:100px;
            height:100px;
            float:left;
            text-align: center;
            border: 2px solid darkgray;
        }
        .odd_div:nth-child(odd),.even_div:nth-child(even)
        {

            background-color: white;
        }
        .odd_div:nth-child(even),.even_div:nth-child(odd)
        {

            background-color: black;
        }
    </style>
</head>
<body>
<div style="border: 5px solid darkgrey;width:68%">
    <?php
        for ($i=1;$i<=8;$i++)
        {
            ?>

            <div>
                <?php
                    for ($j=1;$j<=8;$j++) {
                        if($i%2!=0)
                        {


                        ?>
                        <div class="odd_div"></div>
                        <?php
                        }
                        else
                        {
                        ?>
                            <div class="even_div"></div>
                         <?php
                        }
                    }
                ?>
            </div>

            <div style="clear: both;"></div>
            <?php

        }
    ?>
</div>
</body>
</html>